using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class Distance : BaseObjectScene
{

    [SerializeField] private Transform[] _target;
    [SerializeField] private Text _distanceValue;
    [SerializeField] private float _speed = 5f;

    private const string startDistanceValue = "0";
    private float _posY = 0f;
    private float distanceBetweenObjects;
    private GameObject[] _enemyCount;

    private void Start()
    {
        _distanceValue.text = startDistanceValue;  
    }
    private void Update()
    {
        _enemyCount = GameObject.FindGameObjectsWithTag("Enemy");
        if (_target != null)
        {
            foreach (GameObject item in _enemyCount)
            {
                _target = item.gameObject.GetComponents<Transform>();
                RotateArrow();
                GetDistanceValue();
            }
        }
        if (_target == null)
            Debug.Log("Target is null");

        Debug.DrawLine(transform.position, _target[0].transform.position, Color.red);
    }
  
    private void RotateArrow()
    {
        _posY = transform.position.y;
        _posY = -90f;
        Quaternion rot = Quaternion.LookRotation(new Vector3(_target[0].transform.position.x, _posY, _target[0].transform.position.z) - transform.position);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, _speed * Time.deltaTime);
    }
    private void GetDistanceValue()
    {
        _distanceValue.text = Vector3.Distance(transform.position, _target[0].transform.position).ToString();
        var distance = Vector3.Distance(transform.position, _target[0].transform.position);
        var distanceRound = Math.Round(distance, 2);
        _distanceValue.text = distanceRound.ToString();
    }

}


