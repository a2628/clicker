using UnityEngine;

public class Main : MonoBehaviour
{
    private GameObject _controllersGameObject;
    private InputController _inputController;
    private static Main _instance;
    public static Main Instance { get; private set; }

    void Start()
    {
        Instance = this;
        _controllersGameObject = new GameObject { name = "Controllers" };
        _inputController = _controllersGameObject.AddComponent<InputController>();                 
    }

    public InputController GetInputController
    {
        get => _inputController; 
    }
}
