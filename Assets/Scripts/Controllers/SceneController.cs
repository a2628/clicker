using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : BaseController
{
    [SerializeField] private GameObject[] _enemyArray;
    [SerializeField] private int _enemyAmount = 10;

    private GameObject _enemy;
    private GameObject[] _enemyCount;
    private float _timeBetweenSpawn;
    private float _min = 3f;
    private float _max = 6f;
   
    private void Start()
    {
        StartCoroutine(SpawnEnemies());
        _enemyCount = GameObject.FindGameObjectsWithTag("Enemy");       
    }

    private IEnumerator SpawnEnemies()
    {
        _timeBetweenSpawn = Random.Range(_min, _max);
        yield return new WaitForSeconds(_timeBetweenSpawn);

        if (_enemyCount.Length < _enemyAmount)
        {
            _enemy = Instantiate(_enemyArray[Random.Range(0, _enemyArray.Length)]);
            _enemy.transform.position = new Vector3(Random.Range(6.5f, -10.5f), Random.Range(0.2f, 0.2f), Random.Range(3f,-13f));
            RotateNewEnemy(_enemy);
        }
        GameOver();
        StartCoroutine(SpawnEnemies());
    }

    private void GameOver()
    {
        _enemyCount = GameObject.FindGameObjectsWithTag("Enemy");
        if (_enemyCount.Length == _enemyAmount || _enemyCount.Length > _enemyAmount)
        {
            SceneManager.LoadScene(2);
            StopCoroutine(SpawnEnemies());
        }
    }
    private void RotateNewEnemy(GameObject enemy)
    {
        float angle = Random.Range(0, 360);
        enemy.transform.Rotate(0, angle, 0);
    }
}
