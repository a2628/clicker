using UnityEngine;

public abstract class BaseController : MonoBehaviour
{
    private bool _enabled = false;

    public bool Enabled
    {
        get => _enabled;
        private set => _enabled = value;
    }

    public virtual void On()
    {
        _enabled = true;
    }
    public virtual void Off()
    {
        _enabled = false;
    }
}
