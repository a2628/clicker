using UnityEngine;

public class AudioController : BaseController
{
    [SerializeField] private AudioClip _backgroundSound;

    public AudioClip BackgroundSound => _backgroundSound;

}
