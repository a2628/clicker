using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIEnemy : BaseObjectScene
{
    [SerializeField] private float _speed = 5.0f;
    [SerializeField] private float _obstacleRange = 5.0f;
    [SerializeField] private float _rayRadius = 0.75f;

    private bool _alive;

    void Start()
    {
        _alive = true;
    }

    void Update()
    {
        if (_alive)
        {
            transform.Translate(0, 0, _speed * Time.deltaTime);

            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hit;

            if (Physics.SphereCast(ray, _rayRadius, out hit))
            {
                GameObject hitObject = hit.transform.gameObject;

                if (hit.distance < _obstacleRange)
                {
                    float angle = Random.Range(-110, 110);
                    transform.Rotate(0, angle, 0);
                }
            }
        }
    }
    public void SetAlive(bool alive) => _alive = alive;
}
