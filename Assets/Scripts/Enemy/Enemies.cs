using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Enemies : BaseObjectScene
{
    public abstract float Damage { get; set; }
    public abstract float Health { get; set; }
    public abstract Slider HealthProgress { get; set; }
    public abstract GameObject ExplosionPrefab { get; set; }
    public abstract void OnMouseUp();
    public abstract void ToDeactivateEnemy();
}
