using Assets.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.UI;

public class MonsterHard : Enemies, ISetDamage
{
    [SerializeField] private float _health = 100f;
    [SerializeField] private float _damage = 20f;
    [SerializeField] private Slider _healthProgress;
    [SerializeField] private GameObject _explosionPrefab;
    [SerializeField] private int scoreValue;

    private Score _score;
    private ParticleSystem _explosionParticles;
    private AudioSource _audioSource;
    private bool _isDead = false;

    public override float Damage
    {
        get => _damage;
        set => _damage = value;
    }
    public override float Health
    {
        get => _health;
        set => _health = value;
    }
    public override Slider HealthProgress
    {
        get => _healthProgress;
        set => _healthProgress = value;
    }
    public override GameObject ExplosionPrefab
    {
        get => _explosionPrefab;
        set => _explosionPrefab = value;
    }
    void Start()
    {
        GameObject scoreObj = GameObject.FindWithTag("Score");
        if (scoreObj != null)
            _score = scoreObj.GetComponent<Score>();
        if (scoreObj == null)
            Debug.Log("game object not found");

        _explosionParticles = Instantiate(_explosionPrefab).GetComponent<ParticleSystem>();
        _audioSource = GetComponent<AudioSource>();
        _explosionParticles.gameObject.SetActive(false);
    }
    void Update()
    {
        SetHealthUI(_health);
        if (_isDead)
        {
            Destroy(InstanceObject.GetComponent<Collider>());
            Invoke("ToDeactivateEnemy", 0.5f);
            Destroy(_explosionParticles.gameObject, 1.5f);
            Destroy(InstanceObject, 3f);
        }
    }
    public override void OnMouseUp()
    {
        this.ApplyDamage();
    }
    public override void ToDeactivateEnemy()
    {
        InstanceObject.gameObject.SetActive(false);
    }
    public void ApplyDamage()
    {
        _health -= _damage;
        if (_health <= 0)
            OnDeath();
    }
    private void SetHealthUI(float health)
    {
        _healthProgress.value = _health;      
    }
    private void OnDeath()
    {
        _score.AddScore(scoreValue);

        _explosionParticles.transform.position = transform.position;
        _explosionParticles.gameObject.SetActive(true);
        _explosionParticles.Play();
        _audioSource.Play();
        _isDead = true;
    }
}
