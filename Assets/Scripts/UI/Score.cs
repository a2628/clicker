using UnityEngine;
using UnityEngine.UI;

public class Score : BaseObjectScene
{
    [SerializeField] private Text _scoreText;
    [SerializeField] private Text _enemyText;

    private int _score;
    private GameObject[] _enemyCount;
    
    void Start()
    {
        _score = 0;
        UpdateScore();

    }
    void FixedUpdate()
    {
        _enemyCount = GameObject.FindGameObjectsWithTag("Enemy");
        _enemyText.text = _enemyCount.Length.ToString();
    }
    void UpdateScore()
    {
        _scoreText.text = _score.ToString(); ;
    }
    public void AddScore(int newScoreValue)
    {
        _score += newScoreValue;
        UpdateScore();
    }

}
