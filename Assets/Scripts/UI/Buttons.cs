using System.Collections;
using UnityEngine;

public class Buttons : BaseObjectScene
{
    [SerializeField] GameObject _enemyObj;

    GameObject[] _enemyCount;
    private bool _isFrozen;
    public GameObject EnemyObj => _enemyObj;

    public void OnFreezeEnemies()
    {
        StartCoroutine(FreezeEnemies());       
    }
    public IEnumerator FreezeEnemies()
    {
        _isFrozen = true;
        if (_isFrozen)
        {
            _enemyCount = GameObject.FindGameObjectsWithTag("Enemy");

            foreach (GameObject item in _enemyCount)
            {
                item.gameObject.GetComponent<AIEnemy>().enabled = false;
                var freezeParticle = item.gameObject.GetComponent<ParticleSystem>();
                freezeParticle.Play();
            }
        }

        float waitTime = Time.realtimeSinceStartup + 3;
        yield return new WaitWhile(() => Time.realtimeSinceStartup < waitTime);

        _isFrozen = false;
        if (!_isFrozen)
        {
            _enemyCount = GameObject.FindGameObjectsWithTag("Enemy");

            foreach (GameObject item in _enemyCount)
            {
                item.gameObject.GetComponent<AIEnemy>().enabled = true;
            }
        }
    }

    public void DeathEnemies()
    {
        _enemyCount = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject item in _enemyCount)
        {
            item.gameObject.SetActive(false);
            Destroy(item.gameObject, 7f);
        }       
    }
}
